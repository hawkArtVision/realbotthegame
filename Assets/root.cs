﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
public class root : MonoBehaviour {
    public int currentLevel;
    public int currentXP;

    public Text LevelMenuText;
    public Text LevelText;
    public Slider XPSlider;
    public Text XPText;
    public Text NicknameText;
    public Text NicknameMenuText;

    public int Money;
    public int DestroyedObjects;

    public Text MoneyCounter;
    public Text ObjectsCounterRB;
    public Text ObjectsCounter;
    public int GraphicsLevel;
    public Text GraphicsText;

    public int ScreenSize;
    public Text ScreenSizeText;

    public Text AngryCubeHoursT ; // - Время игры определенным роботом - Angry Cube TEXT
    public Text HammerHoursT; // - Время игры определенным роботом - Hammer TEXT
    public Text ScorpionHoursT; // - Время игры определенным роботом - Scorpion TEXT
    public Text SparkHoursT; // - Время игры определенным роботом - Spark TEXT
    public Text EgerHoursT; // - Время игры определенным роботом - Eger TEXT
    public Text GremlinHoursT; // - Время игры определенным роботом - Gremlin TEXT
    public Text FrankiHoursT; // - Время игры определенным роботом - Franki TEXT
    public Text GGGHoursT; // - Время игры определенным роботом - GGG TEXT

    public Slider AngryCubeHours; // - Время игры определенным роботом - Angry Cube TEXT
    public Slider HammerHours; // - Время игры определенным роботом - Hammer TEXT
    public Slider ScorpionHours; // - Время игры определенным роботом - Scorpion TEXT
    public Slider SparkHours; // - Время игры определенным роботом - Spark TEXT
    public Slider EgerHours; // - Время игры определенным роботом - Eger TEXT
    public Slider GremlinHours; // - Время игры определенным роботом - Gremlin TEXT
    public Slider FrankiHours; // - Время игры определенным роботом - Franki TEXT
    public Slider GGGHours; // - Время игры определенным роботом - GGG TEXT

    public Text VictoryCounter;
    public Text DefeatCounter;
    public Text TimeCounter;

    private MultiwindowMessager MessageWindow;
    public bool godmode;

    public AudioMixer ALLSoundSettings;
    public Slider SoundFX;
    public Slider MusicFX;
    public Slider VoiceFX;

    public Text SoundSliderText;
    public Text MusicSliderText;
    public Text VoiceSliderText;


    public InputField nickchanger;

    public void ResetAllData()
    {
        PlayerPrefs.SetString("Nickname", "Новый Пилот");
        PlayerPrefs.SetInt("Level",1);
        PlayerPrefs.SetInt("XP",0);

        PlayerPrefs.SetInt("AttackDetails", 0);
        PlayerPrefs.SetInt("HealthDetails", 0);
        PlayerPrefs.SetInt("EnergyDetails", 0);

        PlayerPrefs.SetInt("Angry Cube_Attack", 1);
        PlayerPrefs.SetInt("Hammer_Attack", 1);
        PlayerPrefs.SetInt("Scorpion_Attack", 1);
        PlayerPrefs.SetInt("Spark_Attack", 1);
        PlayerPrefs.SetInt("Eger_Attack", 1);
        PlayerPrefs.SetInt("Gremlin_Attack", 1);
        PlayerPrefs.SetInt("Franki_Attack", 1);
        PlayerPrefs.SetInt("GGG_Attack", 1);

        PlayerPrefs.SetInt("Angry Cube_Energy", 1);
        PlayerPrefs.SetInt("Hammer_Energy", 1);
        PlayerPrefs.SetInt("Scorpion_Energy", 1);
        PlayerPrefs.SetInt("Spark_Energy", 1);
        PlayerPrefs.SetInt("Eger_Energy", 1);
        PlayerPrefs.SetInt("Gremlin_Energy", 1);
        PlayerPrefs.SetInt("Franki_Energy", 1);
        PlayerPrefs.SetInt("GGG_Energy", 1);

        PlayerPrefs.SetInt("Angry Cube_Health", 1);
        PlayerPrefs.SetInt("Hammer_Health", 1);
        PlayerPrefs.SetInt("Scorpion_Health", 1);
        PlayerPrefs.SetInt("Spark_Health", 1);
        PlayerPrefs.SetInt("Eger_Health", 1);
        PlayerPrefs.SetInt("Gremlin_Health", 1);
        PlayerPrefs.SetInt("Franki_Health", 1);
        PlayerPrefs.SetInt("GGG_Health", 1);

        PlayerPrefs.SetInt("FirstSkinBought", 0);
        PlayerPrefs.SetInt("SecondSkinBought", 0);
        PlayerPrefs.SetInt("ThirdSkinBought", 0);
        PlayerPrefs.SetInt("ForuthSkinBought", 0);
        PlayerPrefs.SetInt("FifthSkinBought", 0);

        PlayerPrefs.SetInt("Objects", 0);
        PlayerPrefs.SetInt("ObjectsAll", 0);
        PlayerPrefs.SetInt("Money", 0);

        PlayerPrefs.SetInt("Victory", 0);
        PlayerPrefs.SetInt("Defeat", 0);
        PlayerPrefs.SetInt("Time", 0);

        PlayerPrefs.SetInt("Angry CubeHours", 0);
        PlayerPrefs.SetInt("HammerHours", 0);
        PlayerPrefs.SetInt("ScorpionHours", 0);
        PlayerPrefs.SetInt("SparkHours", 0);
        PlayerPrefs.SetInt("EgerHours", 0);
        PlayerPrefs.SetInt("GremlinHours", 0);
        PlayerPrefs.SetInt("FrankiHours", 0);
        PlayerPrefs.SetInt("GGGHours", 0);

        MessageWindow.Message = "Добро пожаловать в REALBOT: The Game";
        MessageWindow.SendMessage();
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);

    }
    void Start() {
        SoundFX.value = PlayerPrefs.GetFloat("Sound");
        MusicFX.value = PlayerPrefs.GetFloat("Music");
        VoiceFX.value = PlayerPrefs.GetFloat("Voice");

        VictoryCounter.text = PlayerPrefs.GetInt("Victory").ToString();
        DefeatCounter.text = PlayerPrefs.GetInt("Defeat").ToString();
            TimeCounter.text = PlayerPrefs.GetInt("Time").ToString() + "МИН";
        if (AngryCubeHoursT != null && HammerHoursT != null && ScorpionHoursT != null && SparkHoursT != null && EgerHoursT != null && GremlinHoursT != null && FrankiHoursT != null && GGGHoursT != null)
        {
            AngryCubeHoursT.text = PlayerPrefs.GetInt("Angry CubeHours").ToString() + "Мин";
            HammerHoursT.text = PlayerPrefs.GetInt("HammerHours").ToString() + "Мин";
            ScorpionHoursT.text = PlayerPrefs.GetInt("ScorpionHours").ToString() + "Мин";
            SparkHoursT.text = PlayerPrefs.GetInt("SparkHours").ToString() + "Мин";
            EgerHoursT.text = PlayerPrefs.GetInt("EgerHours").ToString() + "Мин";
            GremlinHoursT.text = PlayerPrefs.GetInt("GremlinHours").ToString() + "Мин";
            FrankiHoursT.text = PlayerPrefs.GetInt("FrankiHours").ToString() + "Мин";
            GGGHoursT.text = PlayerPrefs.GetInt("GGGHours").ToString() + "Мин";

AngryCubeHours.value = PlayerPrefs.GetInt("Angry CubeHours");
HammerHours.value = PlayerPrefs.GetInt("HammerHours");
ScorpionHours.value = PlayerPrefs.GetInt("ScorpionHours");
SparkHours.value = PlayerPrefs.GetInt("SparkHours");
EgerHours.value = PlayerPrefs.GetInt("EgerHours");
GremlinHours.value = PlayerPrefs.GetInt("GremlinHours");
FrankiHours.value = PlayerPrefs.GetInt("FrankiHours");
GGGHours.value = PlayerPrefs.GetInt("GGGHours");
        }
        ScreenSize = PlayerPrefs.GetInt("ScreenSize");
        GraphicsLevel = PlayerPrefs.GetInt("GraphicsLevel");
        ResolutionGet();
        QualityGet();
    }
    public void Changer()
    {
     /*   PlayerPrefs.SetFloat("Sound", SoundFX.value);
        PlayerPrefs.SetFloat("Music", MusicFX.value);
        PlayerPrefs.SetFloat("Voice", VoiceFX.value);*/
    }
    void Update()
    {
        MessageWindow = GameObject.FindGameObjectWithTag("Message").GetComponent<MultiwindowMessager>();

        ALLSoundSettings.SetFloat("Music", MusicFX.value);
        ALLSoundSettings.SetFloat("Sound", SoundFX.value);
        ALLSoundSettings.SetFloat("Voice", VoiceFX.value);

        SoundSliderText.text = SoundFX.value.ToString();
        MusicSliderText.text = MusicFX.value.ToString();
        VoiceSliderText.text = VoiceFX.value.ToString();

        //
        if (godmode)
        {
            PlayerPrefs.SetInt("Money", 10000000);
            PlayerPrefs.SetInt("Level", 99);
        }
        /* Временный читкод
         * 
         * */
        NicknameMenuText.text = PlayerPrefs.GetString("Nickname");
        NicknameText.text = PlayerPrefs.GetString("Nickname");
        currentLevel = PlayerPrefs.GetInt("Level");
        currentXP = PlayerPrefs.GetInt("XP");

        LevelMenuText.text = currentLevel.ToString();
        LevelText.text = currentLevel.ToString();
        XPSlider.value = currentXP;
        XPSlider.maxValue = 800 * currentLevel;
        XPText.text = XPSlider.value + "/" + XPSlider.maxValue;

        if (currentXP >= XPSlider.maxValue)
        {
            currentLevel++;
            currentXP = 0;
            PlayerPrefs.SetInt("XP", 0);
            PlayerPrefs.SetInt("Level", currentLevel);
            MessageWindow.Message = "ПОЗДРАВЛЯЕМ, У ВАС НОВЫЙ УРОВЕНЬ, ТЕПЕРЬ ВАШ УРОВЕНЬ - " + currentLevel;
            MessageWindow.SendMessage();
        }

        Money = PlayerPrefs.GetInt("Money");
        DestroyedObjects = PlayerPrefs.GetInt("ObjectsAll");
        if (ObjectsCounter != null)
        ObjectsCounter.text = DestroyedObjects.ToString();
        if (ObjectsCounterRB != null)
        ObjectsCounterRB.text = DestroyedObjects.ToString();
        MoneyCounter.text = Money.ToString();
    }


    public void ChangeNick()
    {
        if (nickchanger.text != "")
        {
            NicknameText.text = nickchanger.text;
            PlayerPrefs.SetString("Nickname", nickchanger.text);
            NicknameText.gameObject.SetActive(true);
            nickchanger.gameObject.SetActive(false);
            MessageWindow.Message = "Новое прозвище вам идёт " + nickchanger.text.ToString();
            MessageWindow.SendMessage();
        }
        else
        {
            MessageWindow.Message = "Введите новое прозвище";
            MessageWindow.SendMessage();
        }
    }
    public void EnableChangeNick()
    {
        NicknameText.gameObject.SetActive(false);
        nickchanger.gameObject.SetActive(true);
    }
    public void ScreenSicePlus() {
        if (ScreenSize < 8)
            ScreenSize++;
        PlayerPrefs.SetInt("ScreenSize", ScreenSize);
        ResolutionGet();
    }
    public void MapSet(string mapname)
    {
        if (mapname == "Temple")
        {
            PlayerPrefs.SetString("Map", "Temple");
        }
        if (mapname == "AKD")
        {
            PlayerPrefs.SetString("Map", "AKD");
        }
        if (mapname == "Ruins")
        {
            PlayerPrefs.SetString("Map", "Ruins");
        }
        if (mapname == "Waterfall")
        {
            PlayerPrefs.SetString("Map", "Waterfall");
        }
    }
    public void MapLoad()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(PlayerPrefs.GetString("Map"));
    }
    public void ScreenSiceMinus()
    {
        if (ScreenSize > 0)
            ScreenSize--;
        PlayerPrefs.SetInt("ScreenSize", ScreenSize);
        ResolutionGet();
    }
    public void SettingsGraphPlus() {
        if (GraphicsLevel < 5)
            GraphicsLevel++;
        PlayerPrefs.SetInt("GraphicsLevel", GraphicsLevel);
        QualityGet();
    }
    public void SettingsGraphMinus()
    {
        if (GraphicsLevel > 0)
            GraphicsLevel--;
        PlayerPrefs.SetInt("GraphicsLevel", GraphicsLevel);
        QualityGet();
    }
    public void ResolutionSet() {
       
        if (ScreenSize == 0)
        {
            Screen.SetResolution(640, 480, true);
            MessageWindow.Message = "Разрешение экрана установлено на 640x480";
            MessageWindow.SendMessage();
        }
        if (ScreenSize == 1)
        {
            Screen.SetResolution(800, 600, true);
            MessageWindow.Message = "Разрешение экрана установлено на 800x600";
            MessageWindow.SendMessage();
        }
        if (ScreenSize == 2)
        {
            Screen.SetResolution(1024, 768, true);
            MessageWindow.Message = "Разрешение экрана установлено на 1024x768";
            MessageWindow.SendMessage();
        }
        if (ScreenSize == 3)
        {
            Screen.SetResolution(1280, 720, true);
            MessageWindow.Message = "Разрешение экрана установлено на 1280x720";
            MessageWindow.SendMessage();
        }
        if (ScreenSize == 4)
        {
            Screen.SetResolution(1366, 768, true);
            MessageWindow.Message = "Разрешение экрана установлено на 1366x768";
            MessageWindow.SendMessage();
        }
        if (ScreenSize == 5)
        {
            Screen.SetResolution(1440, 900, true);
            MessageWindow.Message = "Разрешение экрана установлено на 1440x900";
            MessageWindow.SendMessage();
        }
        if (ScreenSize == 6)
        {
            Screen.SetResolution(1600, 900, true);
            MessageWindow.Message = "Разрешение экрана установлено на 1600x900";
            MessageWindow.SendMessage();
        }
        if (ScreenSize == 7)
        {
            Screen.SetResolution(800, 600, true);
            MessageWindow.Message = "Разрешение экрана установлено на 800x600";
            MessageWindow.SendMessage();
        }
        if (ScreenSize == 8)
        {
            Screen.SetResolution(1920, 1280, true);
            MessageWindow.Message = "Разрешение экрана установлено на 1920x1280";
            MessageWindow.SendMessage();
        }
        
    }
    void ResolutionGet()
    {
        if (ScreenSize == 0)
        {
            ScreenSizeText.text = "640x480";
        }
        if (ScreenSize == 1)
        {
            ScreenSizeText.text = "800x600";
        }
        if (ScreenSize == 2)
        {
            ScreenSizeText.text = "1024x768";
        }
        if (ScreenSize == 3)
        {
            ScreenSizeText.text = "1280x720";
        }
        if (ScreenSize == 4)
        {
            ScreenSizeText.text = "1366x768";
        }
        if (ScreenSize == 5)
        {
            ScreenSizeText.text = "1440x900";
        }
        if (ScreenSize == 6)
        {
            ScreenSizeText.text = "1600x900";
        }
        if (ScreenSize == 7)
        {
            ScreenSizeText.text = "1680x1050";
        }
        if (ScreenSize == 8)
        {
            ScreenSizeText.text = "1920x1280";
        }

    }
    void QualityGet()
    {
        if (GraphicsLevel == 0)
        {
            GraphicsText.text = "Низкие";
        }
        if (GraphicsLevel == 1)
        {
            GraphicsText.text = "Производительность";
        }
        if (GraphicsLevel == 2)
        {
            GraphicsText.text = "Умеренные";

        }
        if (GraphicsLevel == 3)
        {
            GraphicsText.text = "Средние";

        }
        if (GraphicsLevel == 4)
        {
            GraphicsText.text = "Высокие";

        }
        if (GraphicsLevel == 5)
        {
            GraphicsText.text = "Ультра";
        }
    }
    public void QualitySet() {
        if (GraphicsLevel == 0)
        {
            QualitySettings.currentLevel = QualityLevel.Fastest;
        }
        if (GraphicsLevel == 1)
        {
            QualitySettings.currentLevel = QualityLevel.Fast;
        }
        if (GraphicsLevel == 2)
        {
            QualitySettings.currentLevel = QualityLevel.Simple;
        }
        if (GraphicsLevel == 3){
            QualitySettings.currentLevel = QualityLevel.Good;
        }
        if (GraphicsLevel == 4){
            QualitySettings.currentLevel = QualityLevel.Beautiful;
        }
        if (GraphicsLevel == 5){
            QualitySettings.currentLevel = QualityLevel.Fantastic;
        }
    }
    
}
