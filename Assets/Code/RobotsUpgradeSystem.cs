﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RobotsUpgradeSystem : MonoBehaviour
{
    public Slider AttackSlider;
    public Slider EnergySlider;
    public Slider HealthSlider;

    public Slider AttackSliderP1;
    public Slider EnergySliderP1;
    public Slider HealthSliderP1;

    public Slider AttackSliderP2;
    public Slider EnergySliderP2;
    public Slider HealthSliderP2;

    public string RobotName;
    public string RobotNameP2;
    public Text ItemText;

    public int DetailsAttack0;
    public int DetailsEnergy1;
    public int DetailsHealth2;

    public Text DetailsAttackAmountText;
    public Text DetailsEnergyAmountText;
    public Text DetailsHealthAmountText;

    [Range(100, 1000)]
    public int priceAttack;

    [Range(100, 1000)]
    public int priceHealth;

    [Range(100, 1000)]
    public int priceEnergy;

    public GameObject Robot;
    public GameObject Parts;
    void Start()
    {
        DetailsAttack0 = PlayerPrefs.GetInt("AttackDetails");
        DetailsEnergy1 = PlayerPrefs.GetInt("EnergyDetails");
        DetailsHealth2 = PlayerPrefs.GetInt("HealthDetails");
        RobotName = PlayerPrefs.GetString("RobotP1");
        HealthSlider.value = PlayerPrefs.GetInt(RobotName + "_Health");
        EnergySlider.value = PlayerPrefs.GetInt(RobotName + "_Energy");
        AttackSlider.value = PlayerPrefs.GetInt(RobotName + "_Attack");
        DetailsAttackAmountText.text = DetailsAttack0.ToString(); /// Обновить текстовые показатели количества
        DetailsEnergyAmountText.text = DetailsEnergy1.ToString();/// Обновить текстовые показатели количества
        DetailsHealthAmountText.text = DetailsHealth2.ToString(); /// Обновить текстовые показатели количества
        Robot.SetActive(true);
        Parts.SetActive(false);
    }
    void Update()
    {
        /* int currentMoney = PlayerPrefs.GetInt("Money");
        if (priceAttack / 2 < currentMoney)
        {
            ItemText.text = "Недостаточно болтов для покупки";
        }
        if (priceEnergy / 2 < currentMoney)
        {
            ItemText.text = "Недостаточно болтов для покупки";
        }
        if (priceHealth / 2 < currentMoney)
        {
            ItemText.text = "Недостаточно болтов для покупки";
        }*/
    }
    public void VisualMain()
    {
        AttackSliderP1.maxValue = AttackSlider.maxValue;
        EnergySliderP1.maxValue = EnergySlider.maxValue;
        HealthSliderP1.maxValue = HealthSlider.maxValue;

        AttackSliderP2.maxValue = AttackSlider.maxValue;
        EnergySliderP2.maxValue = EnergySlider.maxValue;
        HealthSliderP2.maxValue = HealthSlider.maxValue;

    RobotName = PlayerPrefs.GetString("RobotP1");
    AttackSliderP1.value = PlayerPrefs.GetInt(RobotName + "_Attack");
    EnergySliderP1.value = PlayerPrefs.GetInt(RobotName + "_Energy");
    HealthSliderP1.value = PlayerPrefs.GetInt(RobotName + "_Health");

    RobotNameP2 = PlayerPrefs.GetString("RobotP2");
    AttackSliderP2.value = PlayerPrefs.GetInt(RobotNameP2 + "_Attack");
    EnergySliderP2.value = PlayerPrefs.GetInt(RobotNameP2 + "_Energy");
    HealthSliderP2.value = PlayerPrefs.GetInt(RobotNameP2 + "_Health");
    }
    public void Visual(int type)
    {
       bool canbuyA;
        bool canbuyE;
        bool canbuyH;


        int currentMoney = PlayerPrefs.GetInt("Money");
       if (currentMoney < priceAttack / 2 )
        {
            ItemText.text = "Недостаточно болтов для покупки";
            canbuyA = false;
        }
        else
            canbuyA = true;
        if ( currentMoney <priceEnergy / 2 )
        {
            ItemText.text = "Недостаточно болтов для покупки";
            canbuyE = false;
        }
        else
            canbuyE = true;
        if ( currentMoney < priceHealth / 2 )
        {
            ItemText.text = "Недостаточно болтов для покупки";
            canbuyH = false;
        }
        else
            canbuyH = true ;
        Robot.SetActive(true);
        Parts.SetActive(false);
        DetailsAttack0 = PlayerPrefs.GetInt("AttackDetails");
        DetailsEnergy1 = PlayerPrefs.GetInt("EnergyDetails");
        DetailsHealth2 = PlayerPrefs.GetInt("HealthDetails");
        RobotName = PlayerPrefs.GetString("RobotP1");
        HealthSlider.value = PlayerPrefs.GetInt(RobotName + "_Health");
        EnergySlider.value = PlayerPrefs.GetInt(RobotName + "_Energy");
        AttackSlider.value = PlayerPrefs.GetInt(RobotName + "_Attack");
        DetailsAttackAmountText.text = DetailsAttack0.ToString(); /// Обновить текстовые показатели количества
        DetailsEnergyAmountText.text = DetailsEnergy1.ToString();/// Обновить текстовые показатели количества
        DetailsHealthAmountText.text = DetailsHealth2.ToString(); /// Обновить текстовые показатели количества
        if (type == 0 && DetailsAttack0 < 11 && canbuyA)
        {
            ItemText.text = "Вы собираетесь купить [Детали оружия]x1 за - " + (priceAttack / 2) + " болтов, Вы уверены?";
            Robot.SetActive(false);
            Parts.SetActive(true);
        }
        if (type == 1 && DetailsEnergy1 < 11 && canbuyE)
        {
            ItemText.text = "Вы собираетесь купить [Компоненты аккумулятора]x1 за - " + (priceEnergy / 2) + " болтов, Вы уверены?";
            Robot.SetActive(false);
            Parts.SetActive(true);
        }
        if (type == 2 && DetailsHealth2 < 11  && canbuyH)
        {
            ItemText.text = "Вы собираетесь купить [Стальные пластины]x1 за - " + (priceHealth / 2) + " болтов, Вы уверены?";
            Robot.SetActive(false);
            Parts.SetActive(true);
        }
    }
    public void BuyDetails(int type)
    {
        HealthSlider.value = PlayerPrefs.GetInt(RobotName + "_Health");
        EnergySlider.value = PlayerPrefs.GetInt(RobotName + "_Energy");
        AttackSlider.value = PlayerPrefs.GetInt(RobotName + "_Attack");
        RobotName = PlayerPrefs.GetString("RobotP1");

        DetailsAttack0 =  PlayerPrefs.GetInt("AttackDetails");
        DetailsEnergy1 = PlayerPrefs.GetInt("EnergyDetails");
        DetailsHealth2 = PlayerPrefs.GetInt("HealthDetails");

        DetailsAttackAmountText.text = DetailsAttack0.ToString() + "/10"; ; /// Обновить текстовые показатели количества
        DetailsEnergyAmountText.text = DetailsEnergy1.ToString() + "/10"; ;/// Обновить текстовые показатели количества
        DetailsHealthAmountText.text = DetailsHealth2.ToString() + "/10"; ; /// Обновить текстовые показатели количества
        if (type == 0 && DetailsAttack0 < 11)
        {
            ItemText.text = "Вы собираетесь купить [Детали оружия]x1 за - " + (priceAttack / 2) + " болтов, Вы уверены?";
         int currentMoney = PlayerPrefs.GetInt("Money");
         if (currentMoney > priceAttack/2) // Цена это половина стоимости полного апгрейда
         {
             DetailsAttack0++;
             PlayerPrefs.SetInt("AttackDetails", DetailsAttack0);
             currentMoney -= priceAttack / 2;
             PlayerPrefs.SetInt("Money", currentMoney);
             DetailsAttackAmountText.text = DetailsAttack0.ToString() + "/10"; /// Обновить текстовые показатели количества
             Robot.SetActive(true);
             Parts.SetActive(false);
         }
         if (currentMoney < priceAttack / 2  )
         {
             ItemText.text = "Недостаточно болтов для покупки";
             Robot.SetActive(true);
             Parts.SetActive(false);
         }
        }
        if (type == 1 && DetailsEnergy1 < 11)
        {
            ItemText.text = "Вы собираетесь купить [Компоненты аккумулятора]x1 за - " + (priceEnergy / 2) + " болтов, Вы уверены?";
          int currentMoney = PlayerPrefs.GetInt("Money");
          if (currentMoney > priceEnergy / 2)
          {
              DetailsEnergy1++;
              PlayerPrefs.SetInt("EnergyDetails", DetailsEnergy1);
              currentMoney -= priceEnergy / 2;
              PlayerPrefs.SetInt("Money", currentMoney);
              DetailsEnergyAmountText.text = DetailsEnergy1.ToString() + "/10"; /// Обновить текстовые показатели количества
              Robot.SetActive(true);
              Parts.SetActive(false);
          }
          if (currentMoney < priceEnergy / 2)
          {
              ItemText.text = "Недостаточно болтов для покупки";
              Robot.SetActive(true);
              Parts.SetActive(false);
          }
        }
        if (type == 2 && DetailsHealth2 < 11)
        {
            ItemText.text = "Вы собираетесь купить [Стальные пластины]x1 за - " + (priceHealth / 2) + " болтов, Вы уверены?";
          int currentMoney = PlayerPrefs.GetInt("Money");
          if (currentMoney > priceHealth / 2)
          {
              DetailsHealth2++;
              PlayerPrefs.SetInt("HealthDetails", DetailsHealth2);
              currentMoney -= priceHealth / 2;
              PlayerPrefs.SetInt("Money", currentMoney);
              DetailsHealthAmountText.text = DetailsHealth2.ToString() +"/10";  /// Обновить текстовые показатели количества
              Robot.SetActive(true);
              Parts.SetActive(false);
          }
          if ( currentMoney < priceHealth / 2 )
          {
              ItemText.text = "Недостаточно болтов для покупки";
              Robot.SetActive(true);
              Parts.SetActive(false);
          }
        }
        if (DetailsAttack0 == 11)
        {
            int currentLevel = PlayerPrefs.GetInt("Level");
            int cost = priceAttack * currentLevel; // Цена формируется из текущей себестоймости умноженной на уровень
            int currentPower = PlayerPrefs.GetInt(RobotName + "_Attack");
ItemText.text = "Улучшить оружие робота " + RobotName + " с " + currentPower + " ед. урона до " + (currentPower + 10) + " за " + cost + " болтов ?";
            int currentMoney = PlayerPrefs.GetInt("Money");
            
            if (currentMoney > cost)
            {
                currentPower += 10;
                PlayerPrefs.SetInt(RobotName + "_Attack", currentPower);
                currentMoney -= cost;
                PlayerPrefs.SetInt("Money", currentMoney);
                int currentXP = PlayerPrefs.GetInt("XP"); // За каждый апгрейд + 100 ХP очков к уровню Игрока
                currentXP += 100;
                PlayerPrefs.SetInt("XP", currentXP);
                DetailsAttack0 = 0;
                PlayerPrefs.SetInt("AttackDetails", DetailsAttack0);
                AttackSlider.value = PlayerPrefs.GetInt(RobotName + "_Attack");
                DetailsAttackAmountText.text = DetailsAttack0.ToString() + "/10";  /// Обновить текстовые показатели количества
                Robot.SetActive(true);
                Parts.SetActive(false);
            }
        }
        if (DetailsEnergy1 == 11)
        {
            int currentEnergy = PlayerPrefs.GetInt(RobotName + "_Energy");
            int currentMoney = PlayerPrefs.GetInt("Money");
            int currentLevel = PlayerPrefs.GetInt("Level");
            int cost = priceEnergy * currentLevel; // Цена формируется из текущей себестоймости умноженной на уровень
            ItemText.text = "Улучшить восстановление энергии робота " + RobotName + " с " + currentEnergy + " ед. в секунду до " + (currentEnergy + 10) + " за " + cost + " болтов ?";
            if (currentMoney > cost)
            {
                currentEnergy += 10;
                PlayerPrefs.SetInt(RobotName + "_Energy", currentEnergy);
                currentMoney -= cost;
                PlayerPrefs.SetInt("Money", currentMoney);
                int currentXP = PlayerPrefs.GetInt("XP"); // За каждый апгрейд + 100 ХP очков к уровню Игрока
                currentXP += 100;
                PlayerPrefs.SetInt("XP", currentXP);
                DetailsEnergy1 = 0;
                PlayerPrefs.SetInt("EnergyDetails", DetailsEnergy1);
                EnergySlider.value = PlayerPrefs.GetInt(RobotName + "_Energy");
                DetailsEnergyAmountText.text = DetailsEnergy1.ToString() + "/10"; /// Обновить текстовые показатели количества
                Robot.SetActive(true);
                Parts.SetActive(false);
            }
        }
        if (DetailsHealth2 == 11)
        {
            int currentHealth = PlayerPrefs.GetInt(RobotName + "_Health");          
            int currentMoney = PlayerPrefs.GetInt("Money");
            int currentLevel = PlayerPrefs.GetInt("Level");
            int cost = priceHealth * currentLevel; // Цена формируется из текущей себестоймости умноженной на уровень
            ItemText.text = "Улучшить броню робота " + RobotName + " с " + currentHealth + " hp. до " + (currentHealth + 10) + " за " + cost + " болтов ?";
            if (currentMoney > cost)
            {
                currentHealth += 10;
                PlayerPrefs.SetInt(RobotName + "_Health", currentHealth);
                currentMoney -= cost;
                PlayerPrefs.SetInt("Money", currentMoney);
                int currentXP = PlayerPrefs.GetInt("XP"); // За каждый апгрейд + 100 ХP очков к уровню Игрока
                currentXP += 100;
                PlayerPrefs.SetInt("XP", currentXP);
                DetailsHealth2 = 0;
                PlayerPrefs.SetInt("HealthDetails", DetailsHealth2);
                HealthSlider.value = PlayerPrefs.GetInt(RobotName + "_Health");
                DetailsHealthAmountText.text = DetailsHealth2.ToString() + "/10";  /// Обновить текстовые показатели количества
                Robot.SetActive(true);
                Parts.SetActive(false);
            }
        }
    }
    /*
    public void UpgradeAttack(string RobotName)
    {
        int currentMoney = PlayerPrefs.GetInt("Money");
        int currentLevel = PlayerPrefs.GetInt("Level");
        int cost = priceAttack * currentLevel; // Цена формируется из текущей себестоймости умноженной на уровень
        if (currentMoney > cost)
        {
            int currentPower = PlayerPrefs.GetInt(RobotName + "_Attack");
            currentPower += 10;
            PlayerPrefs.SetInt(RobotName + "_Attack", currentPower);
            currentMoney -= cost;
            PlayerPrefs.SetInt("Money", currentMoney);

            int currentXP = PlayerPrefs.GetInt("XP"); // За каждый апгрейд + 100 ХP очков к уровню Игрока
            currentXP += 100;
            PlayerPrefs.SetInt("XP", currentXP);
        }
    }
    public void UpgradeEnergy(string RobotName)
    {
        int currentMoney = PlayerPrefs.GetInt("Money");
        int currentLevel = PlayerPrefs.GetInt("Level");
        int cost = priceEnergy * currentLevel; // Цена формируется из текущей себестоймости умноженной на уровень
        if (currentMoney > cost)
        {
            int currentEnergy = PlayerPrefs.GetInt(RobotName + "_Energy");
            currentEnergy += 10;
            PlayerPrefs.SetInt(RobotName + "_Energy", currentEnergy);
            currentMoney -= cost;
            PlayerPrefs.SetInt("Money", currentMoney);

            int currentXP = PlayerPrefs.GetInt("XP"); // За каждый апгрейд + 100 ХP очков к уровню Игрока
            currentXP += 100;
            PlayerPrefs.SetInt("XP", currentXP);
        }
    }
    public void UpgradeHealth(string RobotName)
    {
        int currentMoney = PlayerPrefs.GetInt("Money");
        int currentLevel = PlayerPrefs.GetInt("Level");
        int cost = priceHealth * currentLevel; // Цена формируется из текущей себестоймости умноженной на уровень
        if (currentMoney > cost)
        {
            int currentHealth = PlayerPrefs.GetInt(RobotName + "_Health");
            currentHealth += 10;
            PlayerPrefs.SetInt(RobotName + "_Health", currentHealth);
            currentMoney -= cost;
            PlayerPrefs.SetInt("Money", currentMoney);

            int currentXP = PlayerPrefs.GetInt("XP"); // За каждый апгрейд + 100 ХP очков к уровню Игрока
            currentXP += 100;
            PlayerPrefs.SetInt("XP", currentXP);
        }
    }*/
}
