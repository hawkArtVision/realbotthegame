﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
public class LoadNextScene : MonoBehaviour {
    public int Seconds;
    public string NextScene;

    public VideoPlayer intro;
    public GameObject eventObject;
	void Start () {
        StartCoroutine(LoadNext());
	}
    void Update()
    {
        if (intro != null && !intro.isPlaying)
        {
            eventObject.SetActive(true);
        }
        if (intro != null && intro.isPlaying)
        {
            eventObject.SetActive(false);
        }

    }
    IEnumerator LoadNext()
    {
        yield return new WaitForSeconds(Seconds);
        UnityEngine.SceneManagement.SceneManager.LoadScene(NextScene);
    }
	
}
