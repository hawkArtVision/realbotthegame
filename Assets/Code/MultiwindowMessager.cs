﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MultiwindowMessager : MonoBehaviour {
    public Text MessageField;
    public Animator Window;
    public string Message;
    public void SendMessage()
    {
        Window.Play("interface_Message");
        MessageField.text = Message;
    }
}
