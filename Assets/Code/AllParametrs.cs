﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllParametrs : MonoBehaviour {


    string Nickname = PlayerPrefs.GetString("Nickname");
    int Level = PlayerPrefs.GetInt("Level"); // - Уровень игрока
    int XP = PlayerPrefs.GetInt("XP"); // - Кол-во XP уровня игрока

    float SoundFX = PlayerPrefs.GetFloat("Sound");
    float MusicFX = PlayerPrefs.GetFloat("Music");
    float VoiceFX = PlayerPrefs.GetFloat("Voice");

    int PowerLevelAngryCube = PlayerPrefs.GetInt("Angry Cube_Attack");
    int PowerLevelHammer = PlayerPrefs.GetInt("Hammer_Attack");
    int PowerLevelScorpion = PlayerPrefs.GetInt("Scorpion_Attack");
    int PowerLevelSpark = PlayerPrefs.GetInt("Spark_Attack");
    int PowerLevelEger = PlayerPrefs.GetInt("Eger_Attack");
    int PowerLevelGremlin = PlayerPrefs.GetInt("Gremlin_Attack");
    int PowerLevelFranki = PlayerPrefs.GetInt("Franki_Attack");
    int PowerLevelGGG = PlayerPrefs.GetInt("GGG_Attack");

    int EnergyLevelAngryCube = PlayerPrefs.GetInt("Angry Cube_Energy");
    int EnergyLevelHammer = PlayerPrefs.GetInt("Hammer_Energy");
    int EnergyLevelScorpion = PlayerPrefs.GetInt("Scorpion_Energy");
    int EnergyLevelSpark = PlayerPrefs.GetInt("Spark_Energy");
    int EnergyLevelEger = PlayerPrefs.GetInt("Eger_Energy");
    int EnergyLevelGremlin = PlayerPrefs.GetInt("Gremlin_Energy");
    int EnergyLevelFranki = PlayerPrefs.GetInt("Franki_Energy");
    int EnergyLevel = PlayerPrefs.GetInt("GGG_Energy");

    int HealthLevelAngryCube = PlayerPrefs.GetInt("Angry Cube_Health");
    int HealthLevelHammer = PlayerPrefs.GetInt("Hammer_Health");
    int HealthLevelScorpion = PlayerPrefs.GetInt("Scorpion_Health");
    int HealthLevelSpark = PlayerPrefs.GetInt("Spark_Health");
    int HealthLevelEger = PlayerPrefs.GetInt("Eger_Health");
    int HealthLevelGremlin = PlayerPrefs.GetInt("Gremlin_Health");
    int HealthLevelFranki = PlayerPrefs.GetInt("Franki_Health");
    int HealthLevelGGG = PlayerPrefs.GetInt("GGG_Health");

            int HealthDetailsAmount = PlayerPrefs.GetInt("HealthDetails"); // - Кол-во купленных деталей здоровья
            int EnergyDetailsAmount = PlayerPrefs.GetInt("EnergyDetails"); // - Кол-во купленных деталей энергии
            int AttackDetailsAmount = PlayerPrefs.GetInt("AttackDetails"); // - Кол-во купленных деталей оружия

            int ScreenSize = PlayerPrefs.GetInt("ScreenSize"); // - Уровень разрешения экрана
            int GraphicsLevel = PlayerPrefs.GetInt("GraphicsLevel"); // - Уровень графики

	        int DestroyedObjects = PlayerPrefs.GetInt("Objects"); // - Кол-во разрушеных объектов на уровне
            int DestroyedObjectsAll = PlayerPrefs.GetInt("ObjectsAll"); // - Кол-во разрушеных объетов суммарно за всю игру
            int Money = PlayerPrefs.GetInt("Money"); // - Кол-во болтов

            int FirstSkinBought = PlayerPrefs.GetInt("FirstSkinBought"); // - Куплен ли первый скин (0,1)
            int SecondSkinBought = PlayerPrefs.GetInt("SecondSkinBought"); // - Куплен ли второй скин (0,1)
            int ThirdSkinBought = PlayerPrefs.GetInt("ThirdSkinBought"); // - Куплен ли третий скин (0,1)
            int ForuthSkinBought = PlayerPrefs.GetInt("ForuthSkinBought"); // - Куплен ли четвёртый скин (0,1)
            int FifthSkinBought = PlayerPrefs.GetInt("FifthSkinBought"); // - Куплен ли пятый скин (0,1)

            int Victory = PlayerPrefs.GetInt("Victory"); // - Кол-во побед
            int Defeat = PlayerPrefs.GetInt("Defeat"); // - Кол-во поражений

            int TimeAll = PlayerPrefs.GetInt("Time"); // - Общее кол-во часов в игре

            int AngryCubeHours = PlayerPrefs.GetInt("Angry CubeHours"); // - Время игры определенным роботом - Angry Cube
            int HammerHours = PlayerPrefs.GetInt("HammerHours"); // - Время игры определенным роботом - Hammer
            int ScorpionHours = PlayerPrefs.GetInt("ScorpionHours"); // - Время игры определенным роботом - Scorpion
            int SparkHours = PlayerPrefs.GetInt("SparkHours"); // - Время игры определенным роботом - Spark
            int EgerHours = PlayerPrefs.GetInt("EgerHours"); // - Время игры определенным роботом - Eger
            int GremlinHours = PlayerPrefs.GetInt("GremlinHours"); // - Время игры определенным роботом - Gremlin
            int FrankiHours = PlayerPrefs.GetInt("FrankiHours"); // - Время игры определенным роботом - Franki
            int GGGHours = PlayerPrefs.GetInt("GGGHours"); // - Время игры определенным роботом - GGG

            string map = PlayerPrefs.GetString("Map"); // - Текущая активная карта

            string robotP1 = PlayerPrefs.GetString("RobotP1"); // - Выбраный активный робот Игрока 1
            string robotP2 = PlayerPrefs.GetString("RobotP2"); // - Выбраный активный робот Игрока 1

            string TwoPlayers =  PlayerPrefs.GetString("TwoPlayers"); // - Два ли игрока на сессии

            string ModeLeague = PlayerPrefs.GetString("Mode"); // - Активный выбраный режим

            
            /*
             * Уникальные номера роботов
             * 0 - Hammer
             * 1 - Scorpion
             * 2 - Spark
             * 3 - Eger
             * 4 - Gremlin
             * 5 - Franki
             * 6 - GGG
             * 7 - Angry Cube
       
             * Уникальные наименования режимов для PlayerPrefs
             * "Meet" - Мясорубка
             * "Classics" - Классика
             * "Single" - Одиночный
             * "League" - Лига чемпионов
             */

            
}
