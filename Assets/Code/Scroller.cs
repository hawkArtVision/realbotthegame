﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scroller : MonoBehaviour {

    public Material skycloud;
    int a;
    void Start()
    {
        skycloud = GetComponent<Material>();
    }
    void Update()
    {
        StartCoroutine(timego());
        skycloud.mainTextureOffset.Set(a,0);
    }
    IEnumerator timego() {
        yield return new WaitForSeconds(1);
        a++;
        StartCoroutine(timego());
    }
}
