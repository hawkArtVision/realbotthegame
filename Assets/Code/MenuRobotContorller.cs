﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MenuRobotContorller : MonoBehaviour {
    public Text RobotNameP1Text;
    public Text RobotNameP2Text;

    public bool TwoPlayersAllowed;
    public GameObject SecondPlayerMenu;
    public GameObject SecondPlayerRobots;
    public GameObject JoinText;
    public GameObject SingleplayerSettings;

    public Slider trapsSlider;
    public Slider randomSlider;
    public Image trapsImage;
    public Image randomImage;
    public Sprite onSprite;
    public Sprite offSprite;

    public GameObject MoneyAll;

    public Text RobotNameSettings;
    public List<GameObject> RobotsList;

    int RobotsListStart;

    public MultiwindowMessager MessageWindow;

    void Start() {
        PlayerPrefs.SetString("RobotP1", "Angry Cube");
        PlayerPrefs.SetString("RobotP2", "Angry Cube");
    }
    public void TwoPlayers(int num)
    {
        if (num == 0)
            TwoPlayersAllowed = false;
        if (num == 1)
            TwoPlayersAllowed = true;
    }
    private void Update()
    {
        MessageWindow = GameObject.FindGameObjectWithTag("Message").GetComponent<MultiwindowMessager>();
        if (RobotNameSettings != null)
        {
            if (PlayerPrefs.GetString("RobotP1") == "Angry Cube")
            {
                RobotNameSettings.text = "Angry Cube";
            }
            if (PlayerPrefs.GetString("RobotP1") == "Hammer")
            {
                RobotNameSettings.text = "Hammer";
            }
            if (PlayerPrefs.GetString("RobotP1") == "Scorpion")
            {
                RobotNameSettings.text = "Scorpion";
            }
            if (PlayerPrefs.GetString("RobotP1") == "Gremlin")
            {
                RobotNameSettings.text = "Gremlin";
            }
            if (PlayerPrefs.GetString("RobotP1") == "Spark")
            {
                RobotNameSettings.text = "Spark";
            }
            if (PlayerPrefs.GetString("RobotP1") == "Eger")
            {
                RobotNameSettings.text = "Eger";
            }
            if (PlayerPrefs.GetString("RobotP1") == "GGG")
            {
                RobotNameSettings.text = "GGG";
            }
            if (PlayerPrefs.GetString("RobotP1") == "Franki")
            {
                RobotNameSettings.text = "Franki";
            }
        }

        if (trapsSlider.value > 0)
            trapsImage.sprite = onSprite;
        else
            trapsImage.sprite = offSprite;
        if (randomSlider.value > 0)
            randomImage.sprite = onSprite;
        else
            randomImage.sprite = offSprite;
        if (SecondPlayerRobots.activeInHierarchy)
            JoinText.SetActive(false);
        if (TwoPlayersAllowed)
        {
            if (!SecondPlayerRobots.activeInHierarchy)
            JoinText.SetActive(true);
            SecondPlayerMenu.SetActive(true);
            if (Input.GetButtonDown("Strikep2"))
            {
                if (MoneyAll != null)
                    MoneyAll.SetActive(false);
                SingleplayerSettings.SetActive(false);
                JoinText.SetActive(false);
                SecondPlayerRobots.SetActive(true);
                PlayerPrefs.SetString("TwoPlayers", "1");
                MessageWindow.Message = "Второй игрок присоеденился к игровой сессии";
                MessageWindow.SendMessage();
            }
        }
        else
        {
            if (MoneyAll != null)
                MoneyAll.SetActive(true);
            SingleplayerSettings.SetActive(true);
            JoinText.SetActive(false);
            SecondPlayerMenu.SetActive(false);
            SecondPlayerRobots.SetActive(false);
            PlayerPrefs.SetString("TwoPlayers", "0");
        }
    }
    public void ModeChosen(string Name) {
        if (Name == "League") {
            TwoPlayersAllowed = false;
        PlayerPrefs.SetString("Mode", "League"); }
        if (Name == "Meet") {
            TwoPlayersAllowed = true;
            PlayerPrefs.SetString("Mode", "Meet");}
        if (Name == "Classics"){
            TwoPlayersAllowed = false;
            PlayerPrefs.SetString("Mode", "Classics");}
        if (Name == "Single"){
            TwoPlayersAllowed = true;
            PlayerPrefs.SetString("Mode", "Single");}
    }
   
    public void RobotChoseP1(string RobotNameP1) {
        PlayerPrefs.SetString("RobotP1", RobotNameP1);
        RobotNameP1Text.text = RobotNameP1;
    }
    public void RobotChoseP2(string RobotNameP2)
    {
        PlayerPrefs.SetString("RobotP2", RobotNameP2);
        RobotNameP2Text.text = RobotNameP2;
    }
}
