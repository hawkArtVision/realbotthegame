﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleObjects : MonoBehaviour {

    public GameObject nonDestroyed;
    public GameObject Destroyed;
    
    void Start()
    {
        nonDestroyed.SetActive(true);
        Destroyed.SetActive(false);
    }
    void OnTriggerEnter(Collider other)
    {
        int curObj;
        int curMon;
        curObj = PlayerPrefs.GetInt("Objects");
        curMon = PlayerPrefs.GetInt("Money");
        if (other.tag == "Damage")
        {
            curMon += 50;
            curObj++;
            PlayerPrefs.SetInt("Money", curMon);
            PlayerPrefs.SetInt("Objects", curObj);
            nonDestroyed.SetActive(false);
            Destroyed.SetActive(true);
            Destroy(Destroyed, 4f);
            Destroy(nonDestroyed, 4f);
        }
    }
}
