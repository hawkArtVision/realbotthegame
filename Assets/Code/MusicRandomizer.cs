﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicRandomizer : MonoBehaviour {

    public List<AudioClip> Song;
    public AudioSource Music;

    void Start()
    {
        int a = Random.RandomRange(0, 10);
        if (a == 0)
            Music.clip = Song[0];
        if (a == 1)
            Music.clip = Song[1];
        if (a == 2)
            Music.clip = Song[2];
        if (a == 3)
            Music.clip = Song[3];
        if (a == 4)
            Music.clip = Song[4];
        if (a == 5)
            Music.clip = Song[5];
        if (a == 6)
            Music.clip = Song[6];
        if (a == 7)
            Music.clip = Song[7];
        if (a == 8)
            Music.clip = Song[8];
        if (a == 9)
            Music.clip = Song[9];
        else
            Music.clip = Song[10];
        Music.Play();
    }
}
