﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class RobotCostumisation : MonoBehaviour
{

    private int Money;
    public Material SparkMain;
    public Material SparkAdd;

    public Material ScorpionMain;
    public Material ScorpionAdd;

    public Material GremlinMain;
    public Material GremlinAdd;

    public Material EgerMain;
    public Material EgerAdd;

    public Material HammerMain;
    public Material HammerAdd;

    public Material FrankyMain;
    public Material FrankyAdd;

    public Material GGGMain;
    public Material GGGAdd;

    public Material ACubeMain;
    public Material ACubeAdd;

    public List<Texture> SkinListMain;
    public List<Texture> SkinListAdd;
    public int TextureSet;

    bool FirstSkinBought;
    bool SecondSkinBought;
    bool ThirdSkinBought;
    bool ForuthSkinBought;
    bool FifthSkinBought;

    public GameObject Bought;
    public GameObject NonBought;

    public MultiwindowMessager MessageWindow;
    public Text Dialogue;

    void Start()
    {
        Money = PlayerPrefs.GetInt("Money"); // Подгрузка текущего кол-ва денег
    }
    void Update()
    {
        MessageWindow = GameObject.FindGameObjectWithTag("Message").GetComponent<MultiwindowMessager>();
        if (TextureSet == 0 && Money <= 7000 && !FirstSkinBought) // Покупка скинов
        {
            Dialogue.text = "Недостаточно болтов";
        }
        if (TextureSet == 1 && Money <= 9000 && !SecondSkinBought)
        {
            Dialogue.text = "Недостаточно болтов";
        }
        if (TextureSet == 2 && Money <= 9100 && !ThirdSkinBought)
        {
            Dialogue.text = "Недостаточно болтов";
        }
        if (TextureSet == 3 && Money <= 9500 && !ForuthSkinBought)
        {
            Dialogue.text = "Недостаточно болтов";
        }
        if (TextureSet == 4 && Money <= 10000 && !FifthSkinBought)
        {
            Dialogue.text = "Недостаточно болтов";
        }

        if (TextureSet == 0 && FirstSkinBought) // Покупка скинов
        {
            Dialogue.text = "Выбран скин 'Кровавая сталь'";
        }
        if (TextureSet == 1 && SecondSkinBought)
        {
            Dialogue.text = "Выбран скин 'Шмель'";
        }
        if (TextureSet == 2 && ThirdSkinBought)
        {
            Dialogue.text = "Выбран скин 'Супергерой'";
        }
        if (TextureSet == 3 && ForuthSkinBought)
        {
            Dialogue.text = "Выбран скин 'Антагонист'";
        }
        if (TextureSet == 4 && FifthSkinBought)
        {
            Dialogue.text = "Выбран скин 'Терминатор'";
        }
    }
    public void TextureSetting(int TextureGet)
    { //Определение номера текстурпака
        TextureSet = TextureGet;
    }
    public void Reset()
    {
        MessageWindow.Message = "Выбран скин по умолчанию";
        MessageWindow.SendMessage();
            ACubeMain.mainTexture = SkinListAdd[5];
            ACubeAdd.mainTexture = SkinListMain[5];
            GremlinAdd.mainTexture = SkinListAdd[5];
            GremlinMain.mainTexture = SkinListMain[5];
            ScorpionAdd.mainTexture = SkinListAdd[5];
            ScorpionMain.mainTexture = SkinListMain[5];
            HammerAdd.mainTexture = SkinListAdd[5];
            HammerMain.mainTexture = SkinListMain[5];
            GGGAdd.mainTexture = SkinListAdd[5];
            GGGMain.mainTexture = SkinListMain[5];
            EgerAdd.mainTexture = SkinListAdd[5];
            EgerMain.mainTexture = SkinListMain[5];
            FrankyAdd.mainTexture = SkinListAdd[5];
            FrankyMain.mainTexture = SkinListMain[5];
            SparkAdd.mainTexture = SkinListAdd[5];
            SparkMain.mainTexture = SkinListMain[5];
    }
    public void Check()
    {
        if (PlayerPrefs.GetInt("FirstSkinBought") == 1)
        {
            FirstSkinBought = true;
            Bought.SetActive(true);
            NonBought.SetActive(false);
        }
        else
        {
            FirstSkinBought = false;
            Bought.SetActive(false);
            NonBought.SetActive(true);
        }
        if (PlayerPrefs.GetInt("SecondSkinBought") == 1)
        {
            SecondSkinBought = true;
            Bought.SetActive(true);
            NonBought.SetActive(false);
        }
        else
        {
            SecondSkinBought = false;
            Bought.SetActive(false);
            NonBought.SetActive(true);
        }
        if (PlayerPrefs.GetInt("ThirdSkinBought") == 1)
        {
            ThirdSkinBought = true;
            Bought.SetActive(true);
            NonBought.SetActive(false);
        }
        else
        {
            ThirdSkinBought = false;
            Bought.SetActive(false);
            NonBought.SetActive(true);
        }
        if (PlayerPrefs.GetInt("ForuthSkinBought") == 1)
        {
            ForuthSkinBought = true;
            Bought.SetActive(true);
            NonBought.SetActive(false);
        }
        else
        {
            ForuthSkinBought = false;
            Bought.SetActive(false);
            NonBought.SetActive(true);
        }
        if (PlayerPrefs.GetInt("FifthSkinBought") == 1)
        {
            FifthSkinBought = true;
            Bought.SetActive(true);
            NonBought.SetActive(false);
        }
        else
        {
            FifthSkinBought = false;
            Bought.SetActive(false);
            NonBought.SetActive(true);
        }
    }

    public void BuySkin() //Покупка скинов
    {
        if (TextureSet == 0 && Money >= 7000 && !FirstSkinBought) // Покупка скинов
        {
            Money -= 7000;
            PlayerPrefs.SetInt("Money", Money);
            FirstSkinBought = true;
            PlayerPrefs.SetInt("FirstSkinBought", 1);
            MessageWindow.Message = "Вы потратили 7000 болтов, поздравляем с покупкой нового скина";
            MessageWindow.SendMessage();
        }
        else
        {
            Dialogue.text = "Недостаточно болтов";
        }
        if (TextureSet == 1 && Money >= 9000 && !SecondSkinBought)
        {
            Money -= 9000;
            PlayerPrefs.SetInt("Money", Money);
            SecondSkinBought = true;
            PlayerPrefs.SetInt("SecondSkinBought", 1);
            MessageWindow.Message = "Вы потратили 9000 болтов, поздравляем с покупкой нового скина";
            MessageWindow.SendMessage();
        }
        else
        {
            Dialogue.text = "Недостаточно болтов";
        }
        if (TextureSet == 2 && Money >= 9100 && !ThirdSkinBought)
        {
            Money -= 9100;
            PlayerPrefs.SetInt("Money", Money);
            ThirdSkinBought = true;
            PlayerPrefs.SetInt("ThirdSkinBought", 1);
            MessageWindow.Message = "Вы потратили 9100 болтов, поздравляем с покупкой нового скина";
            MessageWindow.SendMessage();
        }
        else
        {
            Dialogue.text = "Недостаточно болтов";
        }
        if (TextureSet == 3 && Money >= 9500 && !ForuthSkinBought)
        {
            Money -= 9500;
            PlayerPrefs.SetInt("Money", Money);
            ForuthSkinBought = true;
            PlayerPrefs.SetInt("ForuthSkinBought", 1);
            MessageWindow.Message = "Вы потратили 9500 болтов, поздравляем с покупкой нового скина";
            MessageWindow.SendMessage();
        }
        else
        {
            Dialogue.text = "Недостаточно болтов";
        }
        if (TextureSet == 4 && Money >= 10000 && !FifthSkinBought)
        {
            Money -= 10000;
            PlayerPrefs.SetInt("Money", Money);
            FifthSkinBought = true;
            PlayerPrefs.SetInt("FifthSkinBought", 1);
            MessageWindow.Message = "Вы потратили 10000 болтов, поздравляем с покупкой нового скина";
            MessageWindow.SendMessage();
        }
        else
        {
            Dialogue.text = "Недостаточно болтов";
        }
    }
    public void SkinChange() // Применение скинов
    {
        if (PlayerPrefs.GetString("RobotP1") == "Angry Cube")
        {
            if (TextureSet == 0)
            {
                ACubeMain.mainTexture = SkinListAdd[0];
                ACubeAdd.mainTexture = SkinListMain[0];
            }
            if (TextureSet == 1)
            {
                ACubeMain.mainTexture = SkinListAdd[1];
                ACubeAdd.mainTexture = SkinListMain[1];
            }
            if (TextureSet == 2)
            {
                ACubeMain.mainTexture = SkinListAdd[2];
                ACubeAdd.mainTexture = SkinListMain[2];
            }
            if (TextureSet == 3)
            {
                ACubeMain.mainTexture = SkinListAdd[3];
                ACubeAdd.mainTexture = SkinListMain[3];
            }
            if (TextureSet == 4)
            {
                ACubeMain.mainTexture = SkinListAdd[4];
                ACubeAdd.mainTexture = SkinListMain[4];
            }
        }
        if (PlayerPrefs.GetString("RobotP1") == "Gremlin")
        {
            if (TextureSet == 0)
            {
                GremlinAdd.mainTexture = SkinListAdd[0];
                GremlinMain.mainTexture = SkinListMain[0];
            }
            if (TextureSet == 1)
            {
                GremlinAdd.mainTexture = SkinListAdd[1];
                GremlinMain.mainTexture = SkinListMain[1];
            }
            if (TextureSet == 2)
            {
                GremlinAdd.mainTexture = SkinListAdd[2];
                GremlinMain.mainTexture = SkinListMain[2];
            }
            if (TextureSet == 3)
            {
                GremlinAdd.mainTexture = SkinListAdd[3];
                GremlinMain.mainTexture = SkinListMain[3];
            }
            if (TextureSet == 4)
            {
                GremlinAdd.mainTexture = SkinListAdd[4];
                GremlinMain.mainTexture = SkinListMain[4];
            }
        }
        if (PlayerPrefs.GetString("RobotP1") == "Scorpion")
        {
            if (TextureSet == 0)
            {
                ScorpionAdd.mainTexture = SkinListAdd[0];
                ScorpionMain.mainTexture = SkinListMain[0];
            }
            if (TextureSet == 1)
            {
                ScorpionAdd.mainTexture = SkinListAdd[1];
                ScorpionMain.mainTexture = SkinListMain[1];
            }
            if (TextureSet == 2)
            {
                ScorpionAdd.mainTexture = SkinListAdd[2];
                ScorpionMain.mainTexture = SkinListMain[2];
            }
            if (TextureSet == 3)
            {
                ScorpionAdd.mainTexture = SkinListAdd[3];
                ScorpionMain.mainTexture = SkinListMain[3];
            }
            if (TextureSet == 4)
            {
                ScorpionAdd.mainTexture = SkinListAdd[4];
                ScorpionMain.mainTexture = SkinListMain[4];
            }
        }
        if (PlayerPrefs.GetString("RobotP1") == "Hammer")
        {
            if (TextureSet == 0)
            {
                HammerAdd.mainTexture = SkinListAdd[0];
                HammerMain.mainTexture = SkinListMain[0];
            }
            if (TextureSet == 1)
            {
                HammerAdd.mainTexture = SkinListAdd[1];
                HammerMain.mainTexture = SkinListMain[1];
            }
            if (TextureSet == 2)
            {
                HammerAdd.mainTexture = SkinListAdd[2];
                HammerMain.mainTexture = SkinListMain[2];
            }
            if (TextureSet == 3)
            {
                HammerAdd.mainTexture = SkinListAdd[3];
                HammerMain.mainTexture = SkinListMain[3];
            }
            if (TextureSet == 4)
            {
                HammerAdd.mainTexture = SkinListAdd[4];
                HammerMain.mainTexture = SkinListMain[4];
            }
        }
        if (PlayerPrefs.GetString("RobotP1") == "GGG")
        {
            if (TextureSet == 0)
            {
                GGGAdd.mainTexture = SkinListAdd[0];
                GGGMain.mainTexture = SkinListMain[0];
            }
            if (TextureSet == 1)
            {
                GGGAdd.mainTexture = SkinListAdd[1];
                GGGMain.mainTexture = SkinListMain[1];
            }
            if (TextureSet == 2)
            {
                GGGAdd.mainTexture = SkinListAdd[2];
                GGGMain.mainTexture = SkinListMain[2];
            }
            if (TextureSet == 3)
            {
                GGGAdd.mainTexture = SkinListAdd[3];
                GGGMain.mainTexture = SkinListMain[3];
            }
            if (TextureSet == 4)
            {
                GGGAdd.mainTexture = SkinListAdd[4];
                GGGMain.mainTexture = SkinListMain[4];
            }
        }
        if (PlayerPrefs.GetString("RobotP1") == "Eger")
        {
            if (TextureSet == 0)
            {
                EgerAdd.mainTexture = SkinListAdd[0];
                EgerMain.mainTexture = SkinListMain[0];
            }
            if (TextureSet == 1)
            {
                EgerAdd.mainTexture = SkinListAdd[1];
                EgerMain.mainTexture = SkinListMain[1];
            }
            if (TextureSet == 2)
            {
                EgerAdd.mainTexture = SkinListAdd[2];
                EgerMain.mainTexture = SkinListMain[2];
            }
            if (TextureSet == 3)
            {
                EgerAdd.mainTexture = SkinListAdd[3];
                EgerMain.mainTexture = SkinListMain[3];
            }
            if (TextureSet == 4)
            {
                EgerAdd.mainTexture = SkinListAdd[4];
                EgerMain.mainTexture = SkinListMain[4];
            }
        }
        if (PlayerPrefs.GetString("RobotP1") == "Franki")
        {
            if (TextureSet == 0)
            {
                FrankyAdd.mainTexture = SkinListAdd[0];
                FrankyMain.mainTexture = SkinListMain[0];
            }
            if (TextureSet == 1)
            {
                FrankyAdd.mainTexture = SkinListAdd[1];
                FrankyMain.mainTexture = SkinListMain[1];
            }
            if (TextureSet == 2)
            {
                FrankyAdd.mainTexture = SkinListAdd[2];
                FrankyMain.mainTexture = SkinListMain[2];
            }
            if (TextureSet == 3)
            {
                FrankyAdd.mainTexture = SkinListAdd[3];
                FrankyMain.mainTexture = SkinListMain[3];
            }
            if (TextureSet == 4)
            {
                FrankyAdd.mainTexture = SkinListAdd[4];
                FrankyMain.mainTexture = SkinListMain[4];
            }
        }

        if (PlayerPrefs.GetString("RobotP1") == "Spark")
        {
            if (TextureSet == 0)
            {
                SparkAdd.mainTexture = SkinListAdd[0];
                SparkMain.mainTexture = SkinListMain[0];
            }
            if (TextureSet == 1)
            {
                SparkAdd.mainTexture = SkinListAdd[1];
                SparkMain.mainTexture = SkinListMain[1];
            }
            if (TextureSet == 2)
            {
                SparkAdd.mainTexture = SkinListAdd[2];
                SparkMain.mainTexture = SkinListMain[2];
            }
            if (TextureSet == 3)
            {
                SparkAdd.mainTexture = SkinListAdd[3];
                SparkMain.mainTexture = SkinListMain[3];
            }
            if (TextureSet == 4)
            {
                SparkAdd.mainTexture = SkinListAdd[4];
                SparkMain.mainTexture = SkinListMain[4];
            }
        }

    }
    public void SkinChangeDefault() // Применение скинов
    {
        if (PlayerPrefs.GetString("RobotP1") == "Angry Cube")
        {
            ACubeMain.mainTexture = SkinListAdd[5];
            ACubeAdd.mainTexture = SkinListMain[5];
        }
        if (PlayerPrefs.GetString("RobotP1") == "Gremlin")
        {
            GremlinAdd.mainTexture = SkinListAdd[5];
            GremlinMain.mainTexture = SkinListMain[5];
        }
        if (PlayerPrefs.GetString("RobotP1") == "Scorpion")
        {
            ScorpionAdd.mainTexture = SkinListAdd[5];
            ScorpionMain.mainTexture = SkinListMain[5];
        }
        if (PlayerPrefs.GetString("RobotP1") == "Hammer")
        {

            HammerAdd.mainTexture = SkinListAdd[5];
            HammerMain.mainTexture = SkinListMain[5];
        }
        if (PlayerPrefs.GetString("RobotP1") == "GGG")
        {
            GGGAdd.mainTexture = SkinListAdd[5];
            GGGMain.mainTexture = SkinListMain[5];
        }
        if (PlayerPrefs.GetString("RobotP1") == "Eger")
        {
            EgerAdd.mainTexture = SkinListAdd[5];
            EgerMain.mainTexture = SkinListMain[5];

        }
        if (PlayerPrefs.GetString("RobotP1") == "Franki")
        {
            FrankyAdd.mainTexture = SkinListAdd[5];
            FrankyMain.mainTexture = SkinListMain[5];

        }

        if (PlayerPrefs.GetString("RobotP1") == "Spark")
        {

            SparkAdd.mainTexture = SkinListAdd[5];
            SparkMain.mainTexture = SkinListMain[5];
        }

    }

}