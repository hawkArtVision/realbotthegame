﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationCoping : MonoBehaviour {
    public Transform CopyFrom;
	void Update () {

        if (PlayerPrefs.GetString("TwoPlayers") == "0")
        {
            if (CopyFrom == null)
            CopyFrom = GameObject.FindGameObjectWithTag("TPSCam").transform;
            transform.rotation = CopyFrom.transform.rotation;
        }
        if (PlayerPrefs.GetString("TwoPlayers") == "1")
        {
            transform.rotation = Camera.main.transform.rotation;
        }
	}
}
