﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FightConroller : MonoBehaviour {

    public GameObject Player1;
    public GameObject Player2;

    public GameObject VictoryUI;
    public Image PlayerWon;

    public Text FirstP;
    public Text SecondP;

    public Sprite FirstWon;
    public Sprite SecondWon;


    public BotController firstPlayer;
    public BotController secondPlayer;

    public int FightTimer;

    public Text BattleTime;
    public Text DestroyedObjects;
    public Text BattleDamage;
    public Text MoneyToken;

    public int MoneyGive;

    public GameObject PauseUI;
    public bool Victory;
    public bool Pause;

    public AudioSource Player1Dialogue;
    public AudioSource Player2Dialogue;

    public List<AudioClip> Player1Speech; // 26 фраз первого игрока
    public List<AudioClip> Player2Speech; // 18 фраз второго игрока

    public float Player1StartHealth;
    public float Player2StartHealth;

    public int frazeP1;
    public int frazeP2;
    public GameObject P1MessageShow;
    public GameObject P2MessageShow;
    public int prevSpeech;
    void Start()
    {
        if (Player1 == null)
            Player1 = GameObject.FindGameObjectWithTag("Player");
        if (Player2 == null)
            Player2 = GameObject.FindGameObjectWithTag("Player2");
        if (firstPlayer == null)
            firstPlayer = Player1.GetComponent<BotController>();
        if (secondPlayer == null)
            secondPlayer = Player2.GetComponent<BotController>();
        Player1StartHealth = firstPlayer.Health;
        Player2StartHealth = secondPlayer.Health;
        StartTimer();
        FightTimer = 0;
        VictoryUI.SetActive(false);
    }
    public void StartTimer()
    {
        StartCoroutine(TimerGo());
        int randomFirstSpeech = Random.RandomRange(0, 25);
        Player1Dialogue.clip = Player1Speech[randomFirstSpeech];
        Player1Dialogue.Play();
    }
    public void PauseF()
    {
        Pause = true;
        PauseUI.SetActive(true);
        Time.timeScale = 0;
    }
    public void Resume()
    {
        Pause = false;
        PauseUI.SetActive(false);
        Time.timeScale = 1;
    }
    public void Menu()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
    }
    public void Restart()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Temple");
    }
    public void TwoPlayersContine()
    {
        PlayerPrefs.SetInt("TwoPlayers", 1);
    }
    void WinP1()
    {
        Victory = true;
        StopAllCoroutines();
        int DamageTaken = Mathf.RoundToInt(firstPlayer.Health - secondPlayer.Health);
        BattleTime.text = FightTimer.ToString() + " МИН";
        DestroyedObjects.text = PlayerPrefs.GetInt("Objects").ToString();
        BattleDamage.text = DamageTaken.ToString();
        MoneyGive = PlayerPrefs.GetInt("Objects") + FightTimer + DamageTaken;

        int curMoney;
        curMoney = PlayerPrefs.GetInt("Money");
        curMoney+= MoneyGive;
        PlayerPrefs.SetInt("Money", curMoney);


        MoneyToken.text = MoneyGive.ToString();
        PlayerWon.sprite = FirstWon;
        VictoryUI.SetActive(true);

        Debug.Log("First Player WIN");

        int curVic = PlayerPrefs.GetInt("Victory");
        curVic++;
        PlayerPrefs.SetInt("Victory", curVic);

        int curHours = PlayerPrefs.GetInt(Player1.name + "Hours");
        curHours += FightTimer;
        PlayerPrefs.SetInt(Player1.name + "Hours", curHours);

        int DestroyedObjectsAll = PlayerPrefs.GetInt("Objects") + PlayerPrefs.GetInt("ObjectsAll");
        PlayerPrefs.SetInt("ObjectsAll", DestroyedObjectsAll); // Прибавить кол-во разрушенных объектов к общему значению

        int TimeAll = PlayerPrefs.GetInt("Time") + FightTimer;
        PlayerPrefs.SetInt("Time", TimeAll); // Прибавить кол-во времени к общему значению


        int currentLevel = PlayerPrefs.GetInt("Level");
        int currentXP = PlayerPrefs.GetInt("XP"); // За каждую победу + 100 ХP * текущий уровень Игрока
        currentXP += 100*currentLevel;
        PlayerPrefs.SetInt("XP", currentXP);
    }
    void WinP2()
    {
        Victory = true;
        StopAllCoroutines();
        int DamageTaken = Mathf.RoundToInt(secondPlayer.Health - firstPlayer.Health);
        BattleTime.text = FightTimer.ToString()+" МИН";
        DestroyedObjects.text = PlayerPrefs.GetInt("Objects").ToString();
        BattleDamage.text = DamageTaken.ToString();
        MoneyGive = PlayerPrefs.GetInt("Objects") + FightTimer + DamageTaken;
        MoneyToken.text = MoneyGive.ToString();
        PlayerWon.sprite = SecondWon;
        VictoryUI.SetActive(true);

        int curMoney;
        curMoney = PlayerPrefs.GetInt("Money");
        curMoney += MoneyGive;
        PlayerPrefs.SetInt("Money", curMoney);

        Debug.Log("Second Player WIN");

        int curDef = PlayerPrefs.GetInt("Defeat"); // При победе второго игрока увеличить число поражений
        curDef++;
        PlayerPrefs.SetInt("Defeat", curDef);

        int curHours = PlayerPrefs.GetInt(Player1.name + "Hours"); // После победы увеличить количество часов сыграных 1Р роботом
        curHours += FightTimer;
        PlayerPrefs.SetInt(Player1.name + "Hours", curHours);

        int DestroyedObjectsAll = PlayerPrefs.GetInt("Objects") + PlayerPrefs.GetInt("ObjectsAll");
        PlayerPrefs.SetInt("ObjectsAll", DestroyedObjectsAll); // Прибавить кол-во разрушенных объектов к общему значению

        int TimeAll = PlayerPrefs.GetInt("Time") + FightTimer;
        PlayerPrefs.SetInt("Time", TimeAll); // Прибавить кол-во времени к общему значению
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) && !Pause)
        {
            Pause = true;
        }
        if (Input.GetKeyDown(KeyCode.P) && Pause)
        {
            Pause = false;
        }
        if (Pause)
        {
            PauseF();
        }
        else
        {
            Resume();
        }
        if (Player1 == null)
            Player1 = GameObject.FindGameObjectWithTag("Player");
        if (Player2 == null)
            Player2 = GameObject.FindGameObjectWithTag("Player2");
        if (FirstP == null)
            FirstP.text = GameObject.FindGameObjectWithTag("Player").name;
        if (SecondP == null)
            SecondP.text = GameObject.FindGameObjectWithTag("Player2").name;
        if (firstPlayer == null)
            firstPlayer = Player1.GetComponent<BotController>();
        if (secondPlayer == null)
            secondPlayer = Player2.GetComponent<BotController>();
        if (secondPlayer.Health < 1)
        {
            StartCoroutine(Player1Win());
        }
        if (firstPlayer.Health < 1)
        {
            StartCoroutine(Player2Win());
        }
        if (Player1Dialogue.isPlaying)
            P1MessageShow.SetActive(true);
        else
            P1MessageShow.SetActive(false);
        if (Player2Dialogue.isPlaying)
            P2MessageShow.SetActive(true);
        else
            P2MessageShow.SetActive(false);
    }

    public void RandomSpeechP2()
    {
        int randomFirstSpeech;
        randomFirstSpeech = Random.Range(0, 25);
        if (prevSpeech != randomFirstSpeech)
        {
            if (secondPlayer.Health < Player2StartHealth * 90 / 100 && frazeP2 == 0)
            {
                Player1Dialogue.clip = Player1Speech[randomFirstSpeech];
                prevSpeech = randomFirstSpeech;
                if (!Player1Dialogue.isPlaying)
                    Player1Dialogue.Play();
                frazeP2++;
            }
            if (secondPlayer.Health < Player2StartHealth * 70 / 100 && frazeP2 == 1)
            {
                Player1Dialogue.clip = Player1Speech[randomFirstSpeech];
                prevSpeech = randomFirstSpeech;
                if (!Player1Dialogue.isPlaying)
                    Player1Dialogue.Play();
                frazeP2++;
            }
            if (secondPlayer.Health < Player2StartHealth / 2 && frazeP2 == 2)
            {
                Player1Dialogue.clip = Player1Speech[randomFirstSpeech];
                prevSpeech = randomFirstSpeech;
                if (!Player1Dialogue.isPlaying)
                    Player1Dialogue.Play();
                frazeP2++;
            }
            if (secondPlayer.Health < Player2StartHealth * 30 / 100 && frazeP2 == 3)
            {
                Player1Dialogue.clip = Player1Speech[randomFirstSpeech];
                prevSpeech = randomFirstSpeech;
                if (!Player1Dialogue.isPlaying)
                    Player1Dialogue.Play();
                frazeP2++;
            }
        }
        else
        {
            RandomSpeechP2();
        }
    }
    public void RandomSpeechP1()
    {
        int randomFirstSpeech;
        randomFirstSpeech = Random.Range(0, 25);
        if (prevSpeech != randomFirstSpeech)
        {
            if (firstPlayer.Health < Player1StartHealth * 90 / 100 && frazeP1 == 0)
            {
                Player2Dialogue.clip = Player2Speech[randomFirstSpeech];
                prevSpeech = randomFirstSpeech;
                if (!Player2Dialogue.isPlaying)
                    Player2Dialogue.Play();
                frazeP1++;
            }
            if (firstPlayer.Health < Player1StartHealth * 70 / 100 && frazeP1 == 1)
            {
                Player2Dialogue.clip = Player2Speech[randomFirstSpeech];
                prevSpeech = randomFirstSpeech;
                if (!Player2Dialogue.isPlaying)
                    Player2Dialogue.Play();
                frazeP1++;
            }
            if (firstPlayer.Health < Player1StartHealth / 2 && frazeP1 == 2)
            {
                Player2Dialogue.clip = Player2Speech[randomFirstSpeech];
                prevSpeech = randomFirstSpeech;
                if (!Player2Dialogue.isPlaying)
                    Player2Dialogue.Play();
                frazeP1++;
            }
            if (firstPlayer.Health < Player1StartHealth * 30 / 100 && frazeP1 == 3)
            {
                Player2Dialogue.clip = Player2Speech[randomFirstSpeech];
                prevSpeech = randomFirstSpeech;
                if (!Player2Dialogue.isPlaying)
                    Player2Dialogue.Play();
                frazeP1++;
            }
        }
        else
        {
            RandomSpeechP1();
        }
    }

   
    IEnumerator TimerGo()
    {
        yield return new WaitForSeconds(60);
        FightTimer++;
        StartCoroutine(TimerGo());
    }
    IEnumerator Player1Win()
    {
        yield return new WaitForSeconds(1);
        WinP1();
    }
    IEnumerator Player2Win()
    {
        yield return new WaitForSeconds(1);
        WinP2();
    }
}
