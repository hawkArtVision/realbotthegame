﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RobotSpawner : MonoBehaviour {
    public List<GameObject> Player1;
    public List<GameObject> Player2;

	void Start () {
        // Игрок 1 Бот
        if (PlayerPrefs.GetString("RobotP1") == "Hammer")
        {
            Player1[0].SetActive(true);
        }
        if (PlayerPrefs.GetString("RobotP1") == "Scorpion")
        {
            Player1[1].SetActive(true);
        }
        if (PlayerPrefs.GetString("RobotP1") == "Spark")
        {
            Player1[2].SetActive(true);
        }
        if (PlayerPrefs.GetString("RobotP1") == "Eger")
        {
            Player1[3].SetActive(true);
        }
        if (PlayerPrefs.GetString("RobotP1") == "Gremlin")
        {
            Player1[4].SetActive(true);
        }
        if (PlayerPrefs.GetString("RobotP1") == "Franki")
        {
            Player1[5].SetActive(true);
        }
        if (PlayerPrefs.GetString("RobotP1") == "GGG")
        {
            Player1[6].SetActive(true);
        }
        if (PlayerPrefs.GetString("RobotP1") == "Angry Cube")
        {
            Player1[7].SetActive(true);
        }
        // Игрок 2 Бот
        if (PlayerPrefs.GetString("RobotP2") == "Hammer")
        {
            Player2[0].SetActive(true);
        }
        if (PlayerPrefs.GetString("RobotP2") == "Scorpion")
        {
            Player2[1].SetActive(true);
        }
        if (PlayerPrefs.GetString("RobotP2") == "Spark")
        {
            Player2[2].SetActive(true);
        }
        if (PlayerPrefs.GetString("RobotP2") == "Eger")
        {
            Player2[3].SetActive(true);
        }
        if (PlayerPrefs.GetString("RobotP2") == "Gremlin")
        {
            Player2[4].SetActive(true);
        }
        if (PlayerPrefs.GetString("RobotP2") == "Franki")
        {
            Player2[5].SetActive(true);
        }
        if (PlayerPrefs.GetString("RobotP2") == "GGG")
        {
            Player2[6].SetActive(true);
        }
        if (PlayerPrefs.GetString("RobotP2") == "Angry Cube")
        {
            Player2[7].SetActive(true);
        }
	}
	
	
}
