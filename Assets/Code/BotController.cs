﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BotController : MonoBehaviour {
    public FightConroller baseAsset;
    public List<GameObject> DamageState; //Массив мэшей повреждения робота

    public Dot_Truck_Controller RobotMotor;

    public Image robotPreview;
    public List<Sprite> Previews;

    public Image P1P2detect;
    public Sprite P1;
    public Sprite P2;

    [Range(0,1000)] public float Health; // Здоровье
    [Range(0, 1000)] public float Energy; // Энергия
    [Range(0, 1000)] public float Gas; // Газ

    [Range(0, 1)] public float EnergyReviveSpeed; // Скорость Восстановления Энергии

    private float StartHealth; // Неизменяемый параметр Начального здоровья
    private float StartEnergy; // Неизменяемый параметр Начальной энергии

    public Slider HealthBar; // Слайдер здоровья
    public Slider EnergyBar; // Слайдер энергии
    public Slider GasBar; // Слайдер газа

    public Image EnergyBarFill;
    public Image EnergyBarUIFill;

    public Slider HealthBarUI; // Слайдер здоровья
    public Slider EnergyBarUI; // Слайдер энергии
    public Slider GasBarUI; // Слайдер газа
    public Slider StrikeReadyBarUI; // Слайдер энергии готовности (зажать) сильн. атака

    [Range(0, 100)] public float DamageLow; // Урон слабый (слаб. атака)
    [Range(0, 200)] public float DamageHigh; // Урон сильный (сильн. атака)
    [Range(0, max: 300)] public float DamageVHigh; // Урон особый (зажать сильн. атака)
    private float DamageGet; // Получаемый Урон

    public Animator RobotAnim; // Аниматор робота
    [Range(0, 100)] public float StrikeReady; // Вычисление готовности (зажать) сильн. атака
    public Slider StrikeReadyBar; // Слайдер энергии готовности (зажать) сильн. атака

    public ParticleSystem Sparks; // Партикли получения урона

    public bool Player1;
    public bool Player2; // Обозначение игрока

    public GameObject Weapon; // Оружие робота
    public bool CanRevive; // Кулдаун для возможности перевернуться

    public GameObject ThirdPersonView;

    public AudioSource RobotSound;
    public AudioClip DamageSound;
    public AudioClip AtSound;
    public AudioClip AtSSound;
    public AudioClip AtHSound;
    public GameObject StrikeLoad;

    public Transform ReviveP1;
    public Transform ReviveP2;

    private void Start()
    {
        CanRevive = true;
        RobotSound = GetComponent<AudioSource>();
        if (Player1) { 
        Health += PlayerPrefs.GetInt(PlayerPrefs.GetString("RobotP1") + "_Health");
        EnergyReviveSpeed += PlayerPrefs.GetInt(PlayerPrefs.GetString("RobotP1") + "_Energy");
        DamageHigh += PlayerPrefs.GetInt(PlayerPrefs.GetString("RobotP1") + "_Attack");
        DamageVHigh += PlayerPrefs.GetInt(PlayerPrefs.GetString("RobotP1") + "_Attack");
        DamageLow += PlayerPrefs.GetInt(PlayerPrefs.GetString("RobotP1") + "_Attack");
        }
        if (Player2)
        {
            Health += PlayerPrefs.GetInt(PlayerPrefs.GetString("RobotP2") + "_Health");
            EnergyReviveSpeed += PlayerPrefs.GetInt(PlayerPrefs.GetString("RobotP2") + "_Energy");
            DamageHigh += PlayerPrefs.GetInt(PlayerPrefs.GetString("RobotP2") + "_Attack");
            DamageVHigh += PlayerPrefs.GetInt(PlayerPrefs.GetString("RobotP2") + "_Attack");
            DamageLow += PlayerPrefs.GetInt(PlayerPrefs.GetString("RobotP2") + "_Attack");
        }
        if (PlayerPrefs.GetString("TwoPlayers") == "0")
        {
            if (Player1)
            ThirdPersonView.SetActive(true);
            HealthBar.gameObject.SetActive(false);
            EnergyBar.gameObject.SetActive(false);
            GasBar.gameObject.SetActive(false);
            StrikeReadyBar.gameObject.SetActive(false);
        }
        if (PlayerPrefs.GetString("TwoPlayers") == "1")
        {
            ThirdPersonView.SetActive(false);
            HealthBar.gameObject.SetActive(true);
            EnergyBar.gameObject.SetActive(true);
            GasBar.gameObject.SetActive(true);
            StrikeReadyBar.gameObject.SetActive(true);
        }
        if (Player1)
            P1P2detect.sprite = P1;
        if (Player2)
        {
            P1P2detect.sprite = P2;
            ThirdPersonView.SetActive(false);
        }
        if (gameObject.name == "0)Hammer")
        {
            robotPreview.sprite = Previews[0];
        }
        if (gameObject.name == "1)Scorpion")
        {
            robotPreview.sprite = Previews[1];
        }
        if (gameObject.name == "2)Spark")
        {
            robotPreview.sprite = Previews[2];
        }
        if (gameObject.name == "3)Eger")
        {
            robotPreview.sprite = Previews[3];
        }
        if (gameObject.name == "4)Gremlin")
        {
            robotPreview.sprite = Previews[4];
        }
        if (gameObject.name == "5)Franki")
        {
            robotPreview.sprite = Previews[5];
        }
        if (gameObject.name == "6)GGG")
        {
            robotPreview.sprite = Previews[6];
        }
        if (gameObject.name == "7)Angry Cube")
        {
            robotPreview.sprite = Previews[7];
        }
        baseAsset = GameObject.FindGameObjectWithTag("FightController").GetComponent<FightConroller>();
        RobotMotor = GetComponent<Dot_Truck_Controller>();
        StrikeReady = 0;
        StartHealth = Health;
        StartEnergy = Energy;
        HealthBar.maxValue = StartHealth;
        EnergyBar.maxValue = StartEnergy;
        GasBar.maxValue = Gas;
        HealthBarUI.maxValue = StartHealth;
        EnergyBarUI.maxValue = StartEnergy;
        GasBarUI.maxValue = Gas;
        if (Player1) {
            RobotMotor.moveF = "VerticalP1";
            RobotMotor.moveS = "HorizontalP1";
            RobotMotor.stop = "StopP1";
            Weapon.name = Weapon.name + "_p1";
            Player2 = false; }
        if (Player2) {
            RobotMotor.moveF = "VerticalP2";
            RobotMotor.moveS = "HorizontalP2";
            RobotMotor.stop = "StopP2";
            Weapon.name = Weapon.name + "_p2";
            Player1 = false; }
        EnergyBarFill.color = Color.green;
        EnergyBarUIFill.color = Color.green;
    }

    void Update()
    {

        if (Energy < StartEnergy)
            Energy+= EnergyReviveSpeed; // Восстановление энергии
        if (Player1)
        {
            if (Input.GetButton("Revivep1"))
                Revive();
            if (Input.GetButtonDown("Strikep1")) // Если нажать слаб. атака
                Strike();
            if (Input.GetButton("StrikeHp1") && StrikeReady < 100) // Если зажать сильн. атака
            {
                StrikeReady++;
            }
            if (StrikeReady < 100 && Input.GetButtonUp("StrikeHp1")) // Если отпустил раньше времени - простая сильн. атака
            {
                StrikeS();
                StrikeReady = 0; // Обнулить счётчик
            }
            if (StrikeReady >= 100 && Input.GetButtonUp("StrikeHp1")) // Если дожал до конца - особая сильн. атака
            {
                StrikeH();
                StrikeReady = 0; // Обнулить счётчик
            }
        }
        if (Player2)
        {
            if (Input.GetButton("Revivep2"))
                Revive();
            if (Input.GetButtonDown("Strikep2")) // Если нажать слаб. атака
                Strike();
            if (Input.GetButton("StrikeHp2") && StrikeReady < 100) // Если зажать сильн. атака
            {
                StrikeLoad.SetActive(true);
                StrikeReady++;
            }
            if (StrikeReady < 100 && Input.GetButtonUp("StrikeHp2")) // Если отпустил раньше времени - простая сильн. атака
            {
                StrikeLoad.SetActive(false);
                StrikeS();
                StrikeReady = 0; // Обнулить счётчик
            }
            if (StrikeReady >= 100 && Input.GetButtonUp("StrikeHp2")) // Если дожал до конца - особая сильн. атака
            {
                StrikeLoad.SetActive(false);
                StrikeH();
                StrikeReady = 0; // Обнулить счётчик
            }
        }
        // UI обновление Realtime
        EnergyBar.value = Energy;
        HealthBar.value = Health;
        GasBar.value = Gas;
        StrikeReadyBar.value = StrikeReady;

        EnergyBarUI.value = Energy;
        HealthBarUI.value = Health;
        GasBarUI.value = Gas;
        StrikeReadyBarUI.value = StrikeReady;

        if (EnergyBar.value < EnergyBar.maxValue && EnergyBar.value > (EnergyBar.maxValue * 70) / 100)
        {
            EnergyBarFill.color = Color.green;
            EnergyBarUIFill.color = Color.green;
        }
        if (EnergyBar.value < (EnergyBar.maxValue * 70) / 100 && EnergyBar.value > (EnergyBar.maxValue * 40) / 100)
        {
            EnergyBarFill.color = Color.yellow;
            EnergyBarUIFill.color = Color.yellow;
        }
        if (EnergyBar.value < (EnergyBar.maxValue * 40) / 100 && EnergyBar.value > (EnergyBar.maxValue * 20) / 100)
        {
            EnergyBarFill.color = Color.red;
            EnergyBarUIFill.color = Color.red;
        }
        if (EnergyBar.value < (EnergyBar.maxValue * 20) / 100 && EnergyBar.value > (EnergyBar.maxValue * 10) / 100)
        {
            EnergyBarFill.color = Color.grey;
            EnergyBarUIFill.color = Color.grey;
        }
    }

    public void Revive() {

        if (CanRevive)
        {
            if (Player1)
            transform.rotation = ReviveP1.rotation;
            if (Player2)
            transform.rotation = ReviveP2.rotation;
            CanRevive = false;
            StartCoroutine(RevivingBack());
        }
    }
    IEnumerator RevivingBack() {
        yield return new WaitForSeconds(5);
        CanRevive = true;
    }
    //После того как робот переворчаивается на исходную, еще раз перевернуться он может только через 15 секунд
    public void Strike() {
        if (Energy > DamageLow * 10)
        {
            RobotSound.clip = AtSound;
            RobotSound.Play();
            Energy -= DamageLow * 10;
            Gas -= DamageLow;
            PlayerPrefs.SetFloat("Damage", DamageLow);
            RobotAnim.Play("strike");
            Debug.Log("LowStrike_Damage_" + DamageLow);
        }
        else
            Debug.Log("NeedEnergy");
    }
    public void StrikeH()
    {
        if (Energy > DamageVHigh * 10)
        {
            RobotSound.clip = AtHSound;
            RobotSound.Play();
            Energy -= DamageVHigh * 10;
            Gas -= DamageVHigh;
            PlayerPrefs.SetFloat("Damage", DamageVHigh);
            RobotAnim.Play("strikeH");
            Debug.Log("VeryHeavyStrike_Damage_" + DamageVHigh);
        }
        else
            Debug.Log("NeedEnergy");
    }
    public void StrikeS()
    {
        if (Energy > DamageHigh * 10)
        {
            RobotSound.clip = AtSSound;
            RobotSound.Play();
            Energy -= DamageHigh * 10;
            Gas -= DamageHigh;
            PlayerPrefs.SetFloat("Damage", DamageHigh);
            RobotAnim.Play("strikeS");
            Debug.Log("HeavyStrike_Damage_" + DamageHigh);
        }
        else
            Debug.Log("NeedEnergy");
    }
    void DamageSystem() {
        Sparks.Play();
        if (Health >= StartHealth)
        {
            DamageState[0].SetActive(true);
            DamageState[1].SetActive(false);
            DamageState[2].SetActive(false);
            DamageState[3].SetActive(false);
        }
        if (Health < (StartHealth / 1.2f))
        {
            DamageState[0].SetActive(false);
            DamageState[1].SetActive(true);
            DamageState[2].SetActive(false);
            DamageState[3].SetActive(false);
        }
        if (Health < (StartHealth / 2f))
        {
            DamageState[0].SetActive(false);
            DamageState[1].SetActive(false);
            DamageState[2].SetActive(true);
            DamageState[3].SetActive(false);
        }
        if (Health < (StartHealth / 4))
        {
            DamageState[0].SetActive(false);
            DamageState[1].SetActive(false);
            DamageState[2].SetActive(false);
            DamageState[3].SetActive(true);
        }
    }
    
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Instant")
        {
            Health -= 100000000;
        }
        if (other.tag == "Damage")
        {
            DamageGet = PlayerPrefs.GetFloat("Damage");
            Health -= DamageGet;
            DamageSystem();
            Debug.Log("Damage_From - " + other.name);
            RobotSound.clip = DamageSound;
            RobotSound.Play();
            if (Player1)
            {
                baseAsset.RandomSpeechP1();
            }
            if (Player2)
            {
                baseAsset.RandomSpeechP2();
            }
        }
    }
    public void OnTriggerStay(Collider other)
    {
        if (other.tag == "Damage")
        {
            DamageGet = 1;
            Health -= DamageGet;
            DamageSystem();
            Debug.Log("Damage_From - " + other.name);
        }
    }
}
