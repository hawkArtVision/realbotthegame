﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MenuController : MonoBehaviour {
    public Animator Screen2;
    public Animator Screen1;
    public Animator Switcher;

    public void Scene1()
    {
        Screen2.Play("Disapear");
        Screen1.Play("Apear");
        Switcher.Play("2");
    }
    public void Scene2() {
        Screen1.Play("Disapear");
        Screen2.Play("Apear");
        Switcher.Play("1");
    }
}
