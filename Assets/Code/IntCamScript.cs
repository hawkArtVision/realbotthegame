﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntCamScript : MonoBehaviour {
     public GameObject ThirdPersona;
     public Vector3 Player1Pos;
     public Vector3 Player2Pos;
     public Vector3 center = new Vector3(0,0,0);
     public GameObject centering;
     void Update () {
         ThirdPersona = GameObject.FindGameObjectWithTag("TPSCam");
         if (ThirdPersona != null)
             gameObject.SetActive(false);
         if (ThirdPersona == null)
         {
             gameObject.SetActive(true);
             Player1Pos = GameObject.FindGameObjectWithTag("Player").transform.position;
             Player2Pos = GameObject.FindGameObjectWithTag("Player2").transform.position;
             Vector3 center = ((Player1Pos + Player2Pos) * 0.5f);
             transform.position = center;
             centering.transform.position = center;
             float distance = Vector3.Distance(Player1Pos, Player2Pos) + 10;
             Camera.main.fieldOfView = distance;
         }
    }
}
